---
description: Introduktion till kursen webbutveckling 1
---

# Introduktion

## Centralt innehåll

Undervisningen i kursen ska behandla följande centrala innehåll:

* Webben som plattform, dess historia och samhällspåverkan.
* Teknisk orientering om webbens protokoll, adresser, säkerhet och samspelet mellan klient och server.
* Publikation av webbplatser med och utan webbpubliceringssystem.
* **Processen för ett webbutvecklingsprojekt med målsättningar, planering, specifikation av struktur och design, kodning, optimering, testning, dokumentation och uppföljning.**
* **Märkspråk och deras inbördes roller, syntax och semantik – där det huvudsakliga innehållet är standarderna för HTML och CSS samt orientering om Ecmaskript och dokumentobjektsmodellen (DOM).**
* **Teckenkodning, begrepp, standarder och handhavande.**
* **Bilder och media med alternativa format, optimering och tillgänglighet.**
* **Riktlinjer för god praxis inom webbutveckling.**
* Interoperabilitet genom att följa standarder och testa på olika användaragenter.
* Applikationer som fungerar oberoende av val av användaragent, operativsystem eller hårdvaruplattform och hur tillgänglighet uppnås även för användare med funktionsnedsättning.
* Kvalitetssäkring av applikationens funktion och validering av kodens kvalitet.
* Säkerhet och sätt att identifiera hot och sårbarheter samt hur attacker kan motverkas genom effektiva åtgärder.
* Lagar och andra bestämmelser som styr digital information, till exempel personuppgiftslagen och lagen om elektronisk kommunikation.
* **Terminologi inom området webbutveckling.**

## Bra webbsidor för att fördjupa dig i webbutveckling

* [http://pragmatisk.se](http://pragmatisk.se)
* [https://learntheweb.courses/courses/web-design-1/](https://learntheweb.courses/courses/web-design-1/)
