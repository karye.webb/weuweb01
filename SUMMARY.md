# Table of contents

## Introduktion

* [Introduktion](README.md)
* [Utvecklingsmiljö](introduktion/utvecklingsmiljoe.md)
* [Bra resurser](introduktion/bra-resurser.md)
* [pragmatisk.se](introduktion/pragmatisk-se.md)
* [Arbetsgång i webb](kapitel-1/arbetsgang-i-webb.md)

## Repetition

* [Videor på svenska](repetition/svenska.md)
* [Videor på engelska](repetition/engelska.md)
* [Kurser på engelska](repetition/kurser.md)

## 1. Grunder

* [Pippi Långstrump](kapitel-1/pippi-langstrump.md)
* ["All You Need is Love"](kapitel-1/all-you-need-is-love.md)
* ["Öppna landskap"](kapitel-1/lundell.md)
* [Webbposter 2001](kapitel-1/webbposter.md)
* [Veckomeny](kapitel-1/veckomeny.md)
* [Uppgift: finn alla fel](kapitel-1/finn-alla-fel.md)

## 2. Marginaler

* [Fiji](kapitel-2/reklamsida.md)
* [Uppgift: personporträtt](kapitel-2/person.md)
* [Uppgift: Allsvenskan](kapitel-2/allsvenskan.md)

## 3. div-boxar

* [Mitt visitkort](kapitel-3/visitkort.md)
* [Receptsida för scones](kapitel-3/receptsida.md)
* [Länkade sidor](kapitel-3/laenkade-sidor.md)
* [Bloggen](kapitel-4/blogg.md)
* [Bildgalleri](kapitel-4/bildgalleri.md)
* [E-sport webbsida](kapitel-4/esport.md)
* [Uppgift: Min portfolio](kapitel-4/min-portfolio.md)

## 4. Webbplats

* [Webbplatsen 5 artister](kapitel-5/meny.md)
* [Uppgift: Halloween](kapitel-5/halloween.md)
* [Uppgift: Jul](kapitel-5/jul.md)

## 5. Fördjupning

* [Kontaktformulär](kapitel-6/formular.md)
* [CSS-animation](kapitel-6/css-animation.md)
* [CSS-transition](kapitel-6/css-transition.md)
* [Hamburgermeny](kapitel-6/hamburgermeny.md)
* [Fri hosting](kapitel-10/README.md)
  * [Github Pages](kapitel-10/github-pages.md)
  * [Netlify](kapitel-10/netlify.md)
* [Uppgift: Ekolådan](kapitel-6/ekoladan-formulaer.md)

## 6. Verkliga hemsidor

* [Raspberry Pi Pico](kapitel-7/raspberry-pi-pico.md)
* [Big Burger meny](kapitel-7/burger.md)
* [Youtube grid-videor](kapitel-7/tutorials.md)
* [Uppgift: Zeta produktsida](kapitel-7/zeta-produktsida.md)
* [Uppgift: SVT Play](kapitel-7/svt.md)
* [Uppgifter](kapitel-7/uppgifter.md) 

## 7. Modern webbdesign

* [Semantisk HTML](kapitel-8/semantik.md)
* [Responsiv layout](kapitel-8/responsiv.md)
* [Space Tourism](kapitel-8/space.md)
* [Klassiska layouter 1](kapitel-8/klassiska-layouter-1.md)
* [Klassiska layouter 2](kapitel-8/klassiska-layouter-2.md)
* [Uppgift: Layouter](kapitel-8/uppgift-layouter.md)

## 8. Tester & tillgänglighet

* [Testa webbplatsen](kapitel-11/tester.md)
* [Tillgänglighet](kapitel-11/tillganglighet.md)

## 9. Javascript

* [Javascript intro](kapitel-9/javascript-intro.md)
* [Dark mode switcher](kapitel-9/darkmode.md)
* [Exempel på Javascript](kapitel-9/exempel-pa-javascript.md)

## Projekt

* [Elevarbeten: höstprojekt](kapitel-10/hoestprojekt-halloween/README.md)
  * [Hampus](kapitel-10/hoestprojekt-halloween/hampus-aberg.md)
  * [Carl](kapitel-10/hoestprojekt-halloween/carl-edenflod.md)
  * [Olle](kapitel-10/hoestprojekt-halloween/olle-falk.md)
  * [Gustav](kapitel-10/hoestprojekt-halloween/gustav-walter.md)
