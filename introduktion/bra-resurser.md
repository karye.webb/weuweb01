# Bra resurser

## Sammanfattning av HTML och CSS

Denna webbsida ger en bra överblick över HTML och CSS. Den är bra att ha som referens när du behöver kolla upp något:

* [Pragmatisk.se](https://pragmatisk.se)

En webbsida med bra kvalitetsmaterial om HTML och CSS är:

* [Learn to Code HTML & CSS](https://learn.shayhowe.com/html-css)

## Fria bilder

När du behöver bilder till din webbsida är det bra att använda fria bilder. Här är några webbplatser där du kan hitta fria bilder.

* [https://unsplash.com](https://unsplash.com)

## Fria ikoner

Här är några webbplatser där du kan hitta fria ikoner.

* [https://materialdesignicons.com/](https://materialdesignicons.com)
* [https://fontawesome.com/v5.15/icons?d=gallery\&p=2\&m=free](https://fontawesome.com/v5.15/icons?d=gallery\&p=2\&m=free)
* [https://primer.style/octicons/](https://primer.style/octicons/)


