---
description: Träna grundläggande HTML och CSS genom att skapa en webbsida om en låt.
---

# "Öppna landskap" av Ulf Lundell

I den här övningen ska vi skapa en webbsida om låten "Öppna landskap" av Ulf Lundell. Vi använder grundläggande HTML-taggar för att formatera texten, dvs `h1, p, strong, em, img src`.
I låttexten finns det en radbrytning som vi måste lägga till för att texten ska bli läsbar. Vi använder taggen `<br>` för att göra det.

Vi använder även sk CSS tag-regler för att styla sidan. För varje tagg vi använder skapar vi en CSS-regel. Vi använder även Google Fonts för att välja typsnitt.

## Resultatet

![alt text](../.gitbook/assets/image-106.png)

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **landskap**
* Skapa en fil som heter **index.html**
* Skapa en ny fil som heter **style.css**
* Skapa en mapp **bilder**

## Hitta bilderna

Ladda ned följande bilder och lägg dem i **bilder**-mappen:

![skane.jpg](../.gitbook/assets/skane-1.jpg)

![lundell.png](../.gitbook/assets/lundell-1.jpg)

![lundell.png](../.gitbook/assets/lundell-1.png)

Vad är skillnaden mellan **.jpg** och **.png**?

## Hitta låttexten

Googla låttexten och klistra in den i **index.html**.

## Steg 2 - skapa HTML-sidan

Såhär ser sidans HTML-struktur ut:

* `h1` - en rubrik
* `p` - ett stycke
  * `br` - en radbrytning
  * ... och så vidare
* `p` - ett stycke
    * `br` - en radbrytning
    * ... och så vidare
...
* `img src` - en bild
* `a` - en länk till Wikipedia om Ulf Lundell

Infoga låttexten i **index.html** ooch formatera den med HTML-taggar. Lägg till en radbrytning där det behövs.

![alt text](../.gitbook/assets/image-108.png)

## Steg 3 - styla sidan med CSS

Nu är det dags att styla sidan med CSS. Skapa en CSS-regel för varje tagg vi använder i **index.html**. Använd Google Fonts för att välja typsnitt.

### Alla CSS-regler

Vi skriver ned alla CSS-regler för de taggar vi använder i **index.html**.

![alt text](../.gitbook/assets/image-109.png)

### Välja Google Fonts

Gå till [Google Fonts](https://fonts.google.com/) och välj ett typsnitt. Kopiera länken och infoga den i **index.html**.

* Välj två typsnitt
  * Ett **Display** för rubriken
  * Ett **Serif** för stycken

![Välj Serif i Google Fonts](../.gitbook/assets/image-111.png)

### Styla sidan

Nu är det dags att styla sidan. Skriv ned alla CSS-regler i **style.css**. 

Så här kan det se ut:

![alt text](../.gitbook/assets/image-110.png)

## Uppgift

* Prova att ändra följande CSS-egenskaper:
  * `font-family`
  * `font-size`
  * `color`
  * `background-color`
  * `background-image`
  * `margin-left`
  * `width`
  * `border`
  * `padding`
  * `text-shadow`