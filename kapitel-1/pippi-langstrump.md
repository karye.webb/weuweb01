---
description: Träna redigering i editorn och grundläggande HTML-taggar
---

# Pippi Långstrump
Denna övning ger en introduktion till HTML genom att skapa en webbsida om **Pippi Långstrump**. Vi kommer att använda några grundläggande HTML-taggar, som `h1`, `p`, `strong`, `em`, `img src` och `a`, för att strukturera och formatera innehållet på sidan. Dessa taggar hjälper till att definiera vad olika delar av texten representerar och hur de ska visas i en webbläsare.

## Resultatet
Ditt slutresultat kommer att se ut som bilden nedan. Observera hur de olika HTML-taggarna påverkar textens utseende och layout.

![](../.gitbook/assets/image-33.png)

## Video
För ytterligare förståelse och vägledning, se dessa instruktionsvideor:

### HTML och CSS nybörjarguide del 1

{% embed url="https://youtu.be/8Xp413bo2j4?si=3h1chi4rnnUKioJj" %}

### Stycken, radbrytning och rubriker - HTML och CSS nybörjarguide del 2

{% embed url="https://youtu.be/aoxTnSJSW2I?si=J5sLNN2EMMA3mVbS" %}

### Bilder och bildlänkar - HTML och CSS nybörjarguide del 9

{% embed url="https://youtu.be/uIqUzJA2siE?si=oL7Y72Q_mPMsrAoi" %}

### Länk och länkars utseende - HTML och CSS nybörjarguide del 7

{% embed url="https://youtu.be/AFDjBU1PoLU?si=oO3LVd0wslNKQtdc" %}

## Taggarna

### `h1`-taggen
`h1` står för "Header 1" och används för huvudrubriker på en webbsida. Detta är den största rubriknivån och bör användas för sidans viktigaste titel. Exempel:
```html
<h1>Pippi Långstrump</h1>
```

### `p`-taggen
`p` står för "paragraph" och används för att definiera stycken av text. Det skapar ett synligt avstånd ovanför och under textstycket, vilket gör innehållet lättare att läsa. Exempel:
```html
<p>Pippi Långstrump är en stark och oberoende flicka...</p>
```

### `strong`-taggen
`strong`-taggen används för att markera text som särskilt viktig eller som bör framhävas. Texten blir vanligtvis fetstilad. Exempel:
```html
<strong>Starkast i världen</strong>
```

### `em`-taggen
`em` står för "emphasis" och används för att betona en del av texten, vilket ofta resulterar i kursiverad text. Detta kan användas för att ge särskild betoning på nyckelord eller fraser. Exempel:
```html
<em>Fantastiska äventyr</em>
```

### `img src`-taggen
`img src`-taggen används för att infoga bilder på en webbsida. `src` (source) anger sökvägen till bilden. Det är också bra att inkludera en `alt`-text som beskriver bilden om den inte kan visas. Exempel:
```html
<img src="bilder/pippi.jpg" alt="Pippi Långstrump">
```

### `a`-taggen

`a` står för "anchor" och används för att skapa länkar till andra webbsidor. `href` (hyper reference) anger sökvägen till den länkade sidan. Exempel:
```html
<a href="https://sv.wikipedia.org/wiki/Pippi_L%C3%A5ngstrump">Wikipedia</a>
```

Genom att använda dessa taggar kan vi skapa en strukturerad och tilltalande webbsida. Varje tagg har sin specifika funktion och hjälper till att organisera innehållet på ett sätt som är lätt att följa för både webbläsaren och läsaren.

## Steg 1 - förberedelser - webbrot

1. Texten vi använder kommer från [Pippi Långstrumps Wikipedia-sida](https://sv.wikipedia.org/wiki/Pippi\_L%C3%A5ngstrump).
2. Skapa en mapp på din dator och namnge den **pippi**.
3. Inuti mappen "pippi", skapa en HTML-fil och namnge den **pippi.html**.
4. Skapa även en mapp inuti "pippi"-mappen, och namnge den **bilder**.

## Steg 2 - skapa HTML-sidan

1. Öppna din **pippi.html**-fil i en textredigerare.
1. Skapa en grundläggande HTML5-struktur som visas i bilden nedan och spara filen.![](../.gitbook/assets/image-17.png)
1. Skriv webbsidans titel i `<title>`-taggen. Detta är den text som visas i webbläsarens flik: "Pippi Långstrump". ![](../.gitbook/assets/image-19.png)
1. Kopiera texten från Pippi Långstrumps Wikipedia-sida och klistra in den i din HTML-fil. ![](../.gitbook/assets/image-20.png)
1. Om vi förhandsvisar i webbläsaren nu, kommer det att se ut som bilden nedan. Notera att texten inte är formaterad ännu. ![](../.gitbook/assets/image-21.png)

## Formatera texten så den blir läslig i webbläsaren

1. För att göra texten läslig och välstrukturerad, använder vi HTML-taggar som `h1`, `p`, `strong`, `em`. Dessa taggar hjälper till att strukturera texten och ge den semantisk betydelse. ![](../.gitbook/assets/image-22.png)
1. När vi har lagt till taggarna, kontrollera förhandsvisningen i din webbläsare för att se resultatet. ![](../.gitbook/assets/image-23.png)
1. När man vill "höja rösten" i texten för kan man använda `strong`-taggen. När man vill betona ord inte lika starkt använder man `em`. ![](../.gitbook/assets/image-27.png)
1. Vi fortsätter med en underrubrik och använder `h2`-taggen, och tre stycken med `p`-taggen. ![](../.gitbook/assets/image-28.png)
1. Och vi infogar en bild på Pippi med `img`-taggen. Kom ihåg att ladda ner och placera bilder i din **bilder**-mapp. ![](../.gitbook/assets/image-26.png)
1. Vi fortsätter med några underrubriker och derar stycken, och vi lägger några bilder till. ![](../.gitbook/assets/image-31.png)
1. Avslutningsvis lägger vill en länk till Wikipedia-artikeln om Pippi Långstrump. Det är god ton att "tacka" källan för texten. ![](../.gitbook/assets/image-32.png)

## Uppgift

Skapa en webbsida om dina favoritspel, favoritbok eller favoritfilm. Använd dig av HTML-taggar som `h1`, `p`, `strong`, `em`, `img src` och `a` för att strukturera och formatera innehållet på sidan.

1. Skapa en ny webbrot som heter **favoriter**
1. I den mappen skapar du också en ny mapp som heter **bilder**
1. Ladda ned bilder och lägg dem i mappen **bilder**
1. I webbroten skapar du en HTML-sida som heter **index.html** och skriver innehållet om dina favoriter