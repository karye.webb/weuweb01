# Publicera en webbsida

## FAQ

### Vilka är de vanligaste sätten att publicera en webbsida?

De vanligaste sätten att publicera en webbsida är att använda en webbserver eller en webbhotellstjänst:

* [Github Pages](https://pages.github.com/)
* [Vercel](https://vercel.com/)
* [Netlify](https://www.netlify.com/)
* [Surge](https://surge.sh/)
* [Heroku](https://www.heroku.com/)
* Egen webbserver

### Vad är en webbserver?

En webbserver är en dator som är ansluten till internet och som har en webbserverprogramvara installerad. Webbserverprogramvaran är ett program som kan hantera HTTP-förfrågningar och svara med HTTP-svar. Webbserverprogramvaran kan vara Apache, Nginx eller Microsoft IIS.

### Vad är en webbplats?

En webbplats är en samling webbsidor som är lagrade på en webbserver. En webbplats kan vara en samling statiska HTML-filer eller en samling dynamiska webbsidor som genereras av ett programmeringsspråk som PHP eller Python.

### Egen domän

Med ett eget domännamn kan du använda en adress som är lättare att komma ihåg än en IP-adress. Du kan köpa ett domännamn från en domänleverantör. Det finns många olika domänleverantörer att välja mellan. Några exempel är [Loopia](https://www.loopia.se/), [Binero](https://www.binero.se/), [One.com](https://www.one.com/sv/) och [GoDaddy](https://se.godaddy.com/).

