---
description: Fri webbpublicering från Github.
---

# Github pages

{% embed url="https://youtu.be/QyFcl_Fba-k?si=BxAC_97y6RgIETqr" %}

Om man vill publicera en webbplats gratis på internet så finns det flera alternativ. Ett av de enklaste är att använda tjänsten [Github Pages](https://pages.github.com/).

## Skapa en nytt repository på Github

![alt text](../.gitbook/assets/image-120.png)

* Välj Skapa en nytt **publikt** repository. 
* Välj **Add a README file**.

## Aktivera Github Pages

![alt text](../.gitbook/assets/image-121.png)

* Välj **Settings**.
* Scrolla ner till **Github Pages**.
* Välj **Branch** och välj **main**.
* Välj **Save**.

## publicera

![alt text](../.gitbook/assets/image-122.png)

* Commita och pusha dina filer till ditt repository, som **index.html** och **style.css** mm.
* Till höger under **Deployments** kan du se att din webbplats är publicerad.
* Klicka på **github-pages** för att öppna din webbplats.

![alt text](../.gitbook/assets/image-123.png)