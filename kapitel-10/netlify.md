---
description: Fri webbpublicering från Netlify.
---

# Netlify

{% embed url="https://youtu.be/4h8B080Mv4U?si=R75OrZfqiIYr-F7V" %}

Ett annat alternativ för att publicera en webbplats gratis på internet är att använda tjänsten [Netlify](https://www.netlify.com/).

## Skapa ett konto på Netlify

För att kunna använda Netlify behöver du ett konto. Det är gratis att skapa ett konto och du behöver bara ange ett användarnamn, en e-postadress och ett lösenord.

## Överför filerna till Netlify

* När du har skapat ett konto på Netlify så kan du överföra dina filer till Netlify. Du kan tänka dig att det är som att du överför filer till en USB-sticka. Du kan överföra filer genom att dra och släppa dem eller genom att klicka på en knapp och välja filer.
* Du kan också koppla ditt Netlify-konto till ditt Github-konto och överföra filer direkt från Github.

## Publicera webbplatsen

När du har överfört dina filer till Netlify så kan du publicera din webbplats. Då kommer din webbplats att vara tillgänglig på internet. Du kan välja om du vill att den ska vara tillgänglig på adressen `https://<namnet på ditt repository>.netlify.app` eller om du vill att den ska vara tillgänglig på en egen domän.