---
description: Bygga en tabell lika snygg som på https://www.allsvenskan.se/tabell
---

# Allsvenskans tabell

Nu ska du skapa en snygg tabell för **Allsvenskan** med valfria fotbollsklubbar. 

Du använder grundläggande HTML-taggar för att skapa tabellen, dvs `table`, `<caption>`, `tr`, `th`, `td`.

## Resultat

![](../.gitbook/assets/image-45.png)

## Video

### HTML & CSS - Tabeller - Webbutveckling

{% embed url="https://youtu.be/pEtbDj5pMZk?si=rtFzH4t8NWtJHrP8" %}

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **allsvenskan**
* Skapa en webbsida som heter **tabell.html**
* Skapa en CSS-fil som heter **style.css**

## Steg 2 - skapa HTML-sida

Såhär ser sidans HTML-struktur ut:

* `table` - en tabell
  * `caption` - en rubrik till tabellen
  * `tr` - en rad
    * `th` - en rubrik till en kolumn
    * ...
  * `tr` - en rad
    * `td` - en cell
      * `ìmg` - en bild
    * `td` - en cell
    * ...

### Grundkoden

* Börja med grundkoden
* Fyll i alla HTML-element som bygger upp sidan
* Klubbarnas ikoner kopierar du från [https://www.allsvenskan.se/tabell](https://www.allsvenskan.se/tabell) med högerklick och **Kopiera bildadress**

## Steg 3 - snygga till sidan med CSS

* Du infogar alla CSS-regler som motsvarar de taggar vi använder
* Som bakgrundsbild använd länken [https://allsvenskan.se/wp-content/themes/sef-leagues/images/anniversary-lines.png](https://allsvenskan.se/wp-content/themes/sef-leagues/images/anniversary-lines.png)
* Du stylar tabellen med CSS-egenskaperna `width`, `background`, `font`, `color`, `border`, `padding`, `margin`
* För att får varannan rad i tabellen att ha en annan bakgrundsfärg använder du CSS-regeln:

```css
tr:nth-child(even) {
    background-color: #f2f2f2;
}
```