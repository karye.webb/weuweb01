---
description: Använda klasser för att styla olika
---

# Bloggen

Den här övningen går ut på att skapa en blogg. Vi använder oss av en `div`-box för att skapa en första stor **kontainer** som innehåller andra små `div`-boxar.

Eftersom vi använder `div`-boxar för att skapa kontainern och inläggen behöver vi två olika CSS-regler för att styla dem. Därför använder vi oss av **klasser**. Vi ger varje `div`-box en egen klass. En klass är som en egen "pensel" som vi kan använda för att måla med.

## Resultat

![](../.gitbook/assets/image-55.png)

## Video

### CSS klasser och style-taggen - HTML och CSS nybörjarguide del 5

{% embed url="https://youtu.be/VHa9bFFPtc8?si=Mh_UB4Zhk6R6Sk6r" %}

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **min-blogg**
* Skapa en webbsida som heter **blogg.html**
* Skapa en CSS-fil som heter **style.css**
* Skapa en mapp **bilder**

## Steg 2 - skapa HTML-sidan

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `.kontainer` - en stor box för hela sidan
  * `h1` - en rubrik
  * `.inlagg` - en box för varje inlägg
    * `img` - en bild
    * `h2` - en mellanrubrik
    * `p` - en beskrivning

### Grundkoden

* Börja med grundkoden
* Fyll i alla HTML-element som bygger upp sidan

### Div-boxarna

* Skapa en `div`-box som blir "kontainern" för alla andra `div`-boxar\
  Därför får den klassen `.kontainer`
* Skapa en `div`-box för varje inlägg\
  Därför får de klassen `.inlagg`

![](../.gitbook/assets/image-57.png)

## Steg 3 - snygga till sidan med CSS

### CSS-reglerna

* Infoga först CSS-reset
* Infoga alla CSS-regler som motsvarar de taggar vi använder
* Skapa en ruta med `div`-elementet genom att låsa bredd och höjd

```css	
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
img {
    max-width: 100%;
    height: auto;
}
```

### Välj typsnitt

* Gå till [Google Fonts](https://fonts.google.com) och välj två typsnitt

### Ställ in width, margin och padding

* Ställ in `width` på kontainer till ca 800px
* Centrera kontainer-boxen på webbsidan
* Ställ in `padding` på kontainer-boxen och inlägg-boxarna

### Färgpalett

* Eftersom vi ska använda färger på flera ställen kan vi använda oss av en färgpalett
* På [coolors.co](https://coolors.co/palettes/trending) kan vi hitta snygga färgpaletter

![](../.gitbook/assets/image-54.png)

* Använd färgerna på kontainer-boxen och inlägg-boxarna:

![](../.gitbook/assets/image-56.png)
