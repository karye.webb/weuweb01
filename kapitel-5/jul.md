# Skapa en julwebbplats 🎄

I den här uppgiften ska du skapa en egen webbplats med jul-tema! Webbplatsen ska innehålla tre sidor med teman som julgranar, snögubbar och tomtenissar. Alla sidor ska vara sammankopplade med en meny så att man enkelt kan navigera mellan dem.

## Krav

- Minst tre sidor tex: Julgranar, Snögubbar och Tomtenissar
- **Riktigt och vettigt innehåll** på sidorna
- En meny för att navigera mellan sidorna
- Minst en bild och en beskrivande text på varje sida
- En snygg design med CSS som skapar en julstämning
- Använd CSS flex

## Vad du ska göra

1. **Skapa en meny** som låter dig navigera mellan de tre sidorna: Julgranar, Snögubbar och Tomtenissar.
2. **Bygg minst tre webbsidor**, en för varje tema, med rubriker, bilder och beskrivningar.
3. **Använd CSS** för att skapa en design som förmedlar en julkänsla med färger som rött, grönt, vitt och guld.

## Steg-för-steg-guide

### Steg 1 - förberedelser

1. Skapa en mapp på din dator som heter **julwebbplats**.
2. Skapa tre HTML-filer i mappen och döp dem till t.ex. **julgranar.html**, **snogubbar.html**, **tomtenissar.html**.
3. Skapa en CSS-fil som heter **style.css**.
4. Skapa en mapp i din webbplats som heter **bilder** där du lägger bilder som passar till temat, t.ex. en julgran, en snögubbe och tomtenissar.

### Steg 2 - skapa dina HTML-sidor

Varje sida ska innehålla:

- En rubrik (h1) med temat för sidan, t.ex. "Julgranar".
- En meny (nav) som gör det möjligt att klicka mellan sidorna.
- Figurer (figure) för att gruppera bild och text.
- Länkar (a) till de andra sidor.
- En kort beskrivande text (p).

### Steg 3 - designa med CSS

- Använd en vintrig font från [Google Fonts](https://fonts.google.com), t.ex. en handskriven eller dekorativ stil.
- Lägg till en bakgrund med snöflingor, julkulor eller något annat som påminner om julen.
- Använd CSS-reset för att harmonisera utseendet i olika webbläsare:
    ```css
    html {
        box-sizing: border-box;
    }
    *, *:before, *:after {
        box-sizing: inherit;
    }
    body, h1, p, ul {
        margin: 0;
        padding: 0;
    }
    ```
- Skapa en färgpalett för att fånga julens stämning. [Coolors](https://coolors.co) kan vara till hjälp för inspiration.
- Lägg till skuggor, kantlinjer eller gradienter för att skapa en extra dimension.

## Kul utmaningar

![alt text](../.gitbook/assets/image-118.png)

Här är några konkreta tips och exempelkod för extra utmaningarna, samt länkar till användbara resurser:

### Responsiv design
Använd media queries i CSS för att anpassa layouten till olika skärmstorlekar.

Exempel:
```css
body {
    font-family: Arial, sans-serif;
}

ul {
    display: flex;
    justify-content: center;
    background-color: #d32f2f;
    padding: 10px;
}

ul a {
    margin: 0 10px;
    color: white;
    text-decoration: none;
}

@media (max-width: 600px) {
    ul {
        flex-direction: column;
    }

    ul a {
        margin: 5px 0;
    }
}
```

Resurs:
- [Om Media Queries på W3Schools](https://www.w3schools.com/css/css_rwd_mediaqueries.asp)

### Animationer
CSS-animationer kan ge dynamiska effekter utan att behöva JavaScript.

Exempel: Snöflingor som faller
```html
<div class="snowflake">❄</div>
```

```css
@keyframes fall {
    0% {
        transform: translateY(-10%);
        opacity: 1;
    }
    100% {
        transform: translateY(110%);
        opacity: 0;
    }
}

.snowflake {
    position: absolute;
    top: 0;
    left: 50%;
    font-size: 2em;
    animation: fall 5s linear infinite;
}
```

Resurs:
- [Om CSS Keyframes på W3Schools](https://www.w3schools.com/css/css3_animations.asp)

### Interaktiva element

Skapa interaktivitet med JavaScript för att visa/dölja text, formulär eller andra element.

Exempel:
```html
<button onclick="toggleText()">Visa mer</button>
<p id="hidden-text" style="display: none;">Detta är dold text!</p>
```

```javascript
function toggleText() {
    const text = document.getElementById('hidden-text');
    text.style.display = text.style.display === 'none' ? 'block' : 'none';
}
```

Resurs:
- [om att visa/dölja element på W3Schools](https://www.w3schools.com/howto/howto_js_toggle_hide_show.asp)

### Formulär
Med ett formulär kan besökare skicka in sina julönskningar.

Exempel:
```html
<form>
    <label for="wishlist">Önskelista:</label><br>
    <textarea id="wishlist" name="wishlist" rows="4" cols="50"></textarea><br>
    <button type="submit">Skicka</button>
</form>
```

Styla formuläret i julens färger tex:

```css
form {
    background-color: #f0f8ff; /* Alice Blue */
    padding: 20px;
    border-radius: 10px;
}
label {
    color: #228b22; /* Forest Green */
}
textarea {
    border: 2px solid #228b22;
    border-radius: 5px;
}
button {
    background-color: #dc143c; /* Crimson */
    color: white;
    border: none;
    padding: 5px 10px;
    border-radius: 5px;
}
```

Resurs:
- [HTML Formulär på W3Schools](https://www.w3schools.com/html/html_forms.asp)

### Effekter vid hover
Lägg till hover-effekter på länkar eller bilder för att göra webbplatsen mer interaktiv.

Exempel:
```css
ul a {
    color: white;
    text-decoration: none;
    transition: color 0.3s ease;
}

ul a:hover {
    color: #ffd700; /* Gul som en stjärna */
}
```

### Bakgrundsmusik
Använd `<audio>`-taggen och lägg till kontroller för att låta användaren pausa.

Exempel:
```html
<audio controls autoplay loop>
    <source src="julmusik.mp3" type="audio/mpeg">
    Din webbläsare stöder inte ljuduppspelning.
</audio>
```

Resurs:
- [Om `<audio>`-elementet på W3Schools](https://www.w3schools.com/tags/tag_audio.asp)

### Video
Använd `<video>`-taggen för att infoga en julvideo på din webbplats.

Exempel:
```html
<video width="320" height="240" controls>
    <source src="video.mp4" type="video/mp4">
    Din webbläsare stöder inte video.
</video>
```

Resurs:
- [Om `<video>`-elementet på W3Schools](https://www.w3schools.com/tags/tag_video.asp)

Här är fem roliga och kreativa extra utmaningar eller funktioner som kan göra jul-webbplatsen mer engagerande och visuellt tilltalande:

### Interaktiva julikoner
Lägg till julikoner som ändrar färg eller storlek när man för musen över dem. Till exempel en julgran som lyser eller en tomteluva som hoppar.

Hur:
- Använd [Font Awesome](https://fontawesome.com/) för att hämta ikoner.
- Kombinera med CSS-pseudoklassen `:hover` för effekter.

Exempel:
```html
<i class="fas fa-tree"></i>
```

```css
.fas {
    font-size: 3em;
    color: green;
    transition: transform 0.3s, color 0.3s;
}

.fas:hover {
    transform: scale(1.2);
    color: gold;
}
```

### Ljusslingor som blinkar  
Lägg till en ljusslinga längst upp på sidan där lamporna blinkar i olika färger.

Hur:
- Skapa "lampor" med `<div>`-element och använd CSS-animationer.

Exempel:
```html
<div class="light"></div>
```

```css
.light {
    width: 10px;
    height: 10px;
    background-color: red;
    border-radius: 50%;
    display: inline-block;
    margin: 5px;
    animation: blink 1s infinite alternate;
}

@keyframes blink {
    0% { opacity: 0.5; }
    100% { opacity: 1; }
}
```

### Julklapps-knappar
Skapa knappar som ser ut som inslagna julklappar med CSS, och använd dem för interaktiv navigering eller gömda överraskningar.

Hur:
- Styla knapparna som presenter med CSS, och lägg till interaktivitet.

Exempel:
```html
<button class="present">Öppna mig!</button>
```

```css
.present {
    background-color: #ff0000;
    color: white;
    padding: 20px;
    border: 2px solid #fff;
    border-radius: 5px;
    position: relative;
}

.present:before {
    content: '';
    position: absolute;
    width: 100%;
    height: 10px;
    background-color: gold;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
}
```

### Julgranskulor som flyttar sig när man för musen över dem
Låt julgranskulor (eller andra dekorationer) flytta sig lätt när man rör muspekaren över dem.

Hur:
- Använd CSS-pseudoklassen `:hover` för att flytta kulor.
- Lägg till övergångar för en smidig rörelse.

Exempel:
```html
<div class="ornament"></div>
```

```css
.ornament {
    width: 50px;
    height: 50px;
    background-color: red;
    border-radius: 50%;
    display: inline-block;
    transition: transform 0.3s;
}

.ornament:hover {
    transform: translateY(-10px) rotate(10deg);
}
```

### En julhälsning som "snöar fram"
En text (t.ex. "God Jul!") som sakta syns genom att "falla ner" på sidan likt snöflingor.

Hur:
- Använd `@keyframes` för att animera texten.

Exempel:
```html
<h1 class="snowfall-text">God Jul!</h1>
```

```css
.snowfall-text {
    font-size: 3em;
    color: white;
    animation: fall-text 3s ease-in-out;
}

@keyframes fall-text {
    0% { opacity: 0; transform: translateY(-50px); }
    100% { opacity: 1; transform: translateY(0); }
}
```

### En julkalender med luckor
Lägg till en interaktiv julkalender där man kan klicka på luckor för att få en överraskning.

Hur:
- Skapa en tabell eller ett grid där varje "lucka" representerar en dag.
- Använd JavaScript för att öppna luckorna.

Exempel:
```html
<div class="calendar">
    <div class="day" onclick="openDay(this)">1</div>
    <div class="day" onclick="openDay(this)">2</div>
</div>
```

```css
.calendar {
    display: grid;
    grid-template-columns: repeat(5, 50px);
    gap: 10px;
}

.day {
    background-color: #ff0000;
    color: white;
    padding: 20px;
    text-align: center;
    cursor: pointer;
    border: 2px solid #fff;
}

.day.open {
    background-color: #00ff00;
    color: black;
}
```

```javascript
function openDay(day) {
    day.classList.add('open');
    day.innerText = "🎁";
}
```

### omten som följer musen
En tomteikon som "följer" muspekaren runt sidan.

Hur:
- Använd JavaScript för att positionera tomten baserat på musens rörelse.

Exempel:
```html
<div id="santa">🎅</div>
```

```css
#santa {
    position: absolute;
    font-size: 2em;
    pointer-events: none;
    transform: translate(-50%, -50%);
}
```

```javascript
document.addEventListener('mousemove', (event) => {
    const santa = document.getElementById('santa');
    santa.style.top = `${event.clientY}px`;
    santa.style.left = `${event.clientX}px`;
});
```

### En popup-julklapp
När man besöker sidan, öppnas en popup med en julklapp som innehåller ett meddelande eller en överraskning.

Hur:
- Skapa en dold popup som visas med JavaScript.

Exempel:
```html
<div id="popup" class="hidden">
    <div class="gift-box">🎁</div>
    <p>God Jul! Öppna presenten för att se ditt meddelande!</p>
</div>
```

```css
#popup {
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: white;
    border: 2px solid red;
    padding: 20px;
    text-align: center;
    display: none;
}

.hidden {
    display: none;
}
```

```javascript
window.onload = () => {
    document.getElementById('popup').style.display = 'block';
};
```

### Interaktiva ljuseffekter på rubriker
Lägg till en skimrande ljuseffekt på rubriker som ser ut som en lysande neon-skylt.

Hur:
- Använd `text-shadow` och animationer.

Exempel:
```css
h1 {
    font-size: 3em;
    color: red;
    text-shadow: 0 0 10px red, 0 0 20px red, 0 0 30px orange;
    animation: neon 1.5s infinite alternate;
}

@keyframes neon {
    0% { text-shadow: 0 0 10px red, 0 0 20px red, 0 0 30px orange; }
    100% { text-shadow: 0 0 20px red, 0 0 30px orange, 0 0 40px yellow; }
}
```

Dessa idéer ger eleverna möjlighet att experimentera med CSS, JavaScript och design på kreativa sätt! 🎄✨