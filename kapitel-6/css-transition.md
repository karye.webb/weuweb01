# CSS transitions

## Vad är CSS transitions?

CSS transitions används för att skapa animeringar genom att göra förändringar av en CSS-egenskap gradvis över tid. Det är ett enkelt sätt att förbättra användarupplevelsen genom att göra förändringar mjuka och visuellt tilltalande.

### Exempel på användningsområden
- Färgändringar på knappar vid hover.
- Mjuka rörelser för element som växer eller krymper.
- Skuggor som gradvis blir synliga.

## Syntax för CSS transitions

Grundläggande syntax:
```css
transition: [egenskap] [varaktighet] [timing-funktion] [fördröjning];
```

### Parametrar

1. **Egenskap (property)**: Vilken CSS-egenskap som ska animeras (t.ex. `background-color`, `transform`).
2. **Varaktighet (duration)**: Hur lång tid animationen ska ta (t.ex. `0.5s`, `2s`).
3. **Timing-funktion (timing-function)**: Bestämmer hur animationen utvecklas över tid (t.ex. `ease`, `linear`, `ease-in-out`).
4. **Fördröjning (delay)** *(valfritt)*: Hur lång tid som ska gå innan animationen startar (t.ex. `0.2s`).

### Exempel
```css
.box {
    background-color: blue;
    transition: background-color 0.5s ease;
}

.box:hover {
    background-color: red;
}
```

## CSS transitions i praktiken

### 1. Ändra färg

```css
.knapp {
    background-color: #007BFF;
    color: white;
    padding: 10px 20px;
    border: none;
    border-radius: 5px;
    cursor: pointer;
    transition: background-color 0.3s ease;
}

.knapp:hover {
    background-color: #0056b3;
}
```

### 2. Skala ett element

```css
.box {
    width: 100px;
    height: 100px;
    background-color: teal;
    transition: transform 0.3s ease;
}

.box:hover {
    transform: scale(1.2); /* Skala upp elementet */
}
```

### 3. Lägg till en skugga

```css
.kort {
    width: 200px;
    height: 100px;
    background-color: #f0f0f0;
    border: 1px solid #ddd;
    box-shadow: 0 0 0 rgba(0, 0, 0, 0);
    transition: box-shadow 0.4s ease-in-out;
}

.kort:hover {
    box-shadow: 0 5px 15px rgba(0, 0, 0, 0.3);
}
```

## Timing-funktioner

Timing-funktioner bestämmer hur animationen känns. Här är några vanliga alternativ:

| Timing-funktion | Beskrivning |
|------------------|-------------|
| `ease`          | Startar långsamt, går snabbare och avslutar långsamt. |
| `linear`        | Går i samma hastighet hela tiden. |
| `ease-in`       | Startar långsamt och blir snabbare. |
| `ease-out`      | Startar snabbt och blir långsammare. |
| `ease-in-out`   | Startar och slutar långsamt. |

### Exempel
```css
.box {
    transition: all 0.5s ease-in-out;
}
```

## Uppgifter

### Uppgift 1 – Enkel färgövergång
Skapa en knapp som ändrar bakgrundsfärg med en mjuk övergång när du hovrar över den.

### Uppgift 2 – Växande ruta
Skapa en kvadrat som blir större och ändrar färg när användaren hovrar över den.

### Uppgift 3 – Skuggor och rörelse
Skapa ett kort som får en skugga och flyttar sig något uppåt när du hovrar över det.

## Experimentera med transitions

Testa själv genom att kombinera olika egenskaper och parametrar för att skapa unika animationer. Prova att använda `transition` på följande egenskaper:

- `opacity`
- `width` och `height`
- `border-radius`
- `background-color` och `color`

Vill du ha en labb med färdiga steg eller ytterligare material, till exempel videos och kodexempel?