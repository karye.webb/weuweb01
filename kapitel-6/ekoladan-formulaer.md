---
description: Träna på att skapa en webbsida lik som i verkligheten.
---

# Uppgift - Ekolådan

I den här uppgiften ska du försöka skapa en webbsida som liknar Ekolådans formulär. 

![Skärmdump på kontakformuläret](../.gitbook/assets/dump-ekoladan.png)

## Syfte

* Träna på att formulärs grundläggande delar
* Träna på styla formulär
* Träna på CSS grid

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **ekoladan**
* Skapa en webbsida som heter **kontakt.html**
* Skapa en CSS-fil som heter **kontakt.css**
* Skapa en mapp **bilder**

## Steg 2 - skapa HTML-sidan

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `.kontainer` - en stor box för hela sidan
  * `h1` - en rubrik
  * `p` - en beskrivning
  * `form` - ett formulär
    * `label` - en etikett till ett fält
    * `input` - ett fält där användaren kan skriva in något
    * ..
    * `textarea` - ett större fält där användaren kan skriva in något
    * `button` - en knapp

### Grundkoden

* Börja med grundkoden
* Fyll i alla HTML-element som bygger upp sidan
* Så här ett formulär ut: [https://devdocs.io/html/element/form](https://devdocs.io/html/element/form)

### Text på sidan

> Du når oss på telefon 08-554 810 80. Våra telefontider är måndag-fredag mellan klockan 10 och 12 och på tisdagar även mellan 13 och 15. Vid andra tider är det säkraste sättet att komma i kontakt med oss att skicka ett meddelande till info@ekoladan.se eller genom att använda formuläret nedan.

## Steg 3 - snygga till sidan med CSS

### CSS-reglerna

* Infoga först CSS-reset
* Infoga alla CSS-regler som motsvarar de taggar vi använder
* Välj **Courrier** som typsnitt

```css	
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
img {
    max-width: 100%;
    height: auto;
}
```

### Bakgrundsbilden

![bg.jpg](../.gitbook/assets/bg.jpg)
