---
description: Med CSS grid är det lätt att skapa kolumner.
---

# Big Burger meny

Här är en till övning på att återskapa en autentisk webbsida. Vi ska återskapa menyn och rutnätet med hamburgare.

Vi använder oss av en **kontainer** som innehåller flera **boxar** brevid varandra horisontellt. Vi använder också `flex` för att skapa ett horisontellt sidhuvud.

![Exempel på indelning](../.gitbook/assets/dump-burger-1.png)

* `div.kontainer .rutnät` - en stor box som innehåller alla andra boxar
  * `display: grid;` - gör att boxarna ligger bredvid varandra
  * `grid-template-columns: 1fr 1fr 1fr;` - ställer in tex tre delar lika stora
  * `grid-gap: 20px;` - ställer in mellanrummet mellan boxarna

På sidhuvudet använder vi `flex` för att skapa en horisontell layout:

* `div.kontainer .sidhuvud` - en box för sidhuvudet
  * `display: flex;` - gör att logga och meny ligger bredvid varandra
  * `justify-content: space-between;` - sprider ut delarna jämnt

## Resultat

![Återskapad med CSS grid](<../.gitbook/assets/image (69).png>)

## Video

### Flex vs Grid

{% embed url="https://youtu.be/hT9ABJyOzwM?si=w5bAhnZXbHIhZsJY" %}

## Spelet CSS Grid Garden

Testa även [CSS Grid Garden](https://cssgridgarden.com/#sv) för att lära dig CSS grid.

## Steg 1 - förberedelser - webbrot

* Skapa en mapp som heter **burger-king**
* Skapa en webbsida som heter **meny.html**
* Skapa en CSS-fil som heter **style.css**
* Skapa en mapp **bilder**

## Steg 2 - skapa HTML-sidan

### HTML-strukturen

Såhär ser sidans HTML-struktur ut:

* `.kontainer` - en stor box för hela sidan
  * `.sidhuvud` - en box för sidhuvudet
    * `img` - en logga
    * `ul` - en lista med menyval
      * `li` - ett menyval
  * `.rutnät` - en box för alla produkter
    * `.produkt` - en box för varje produkt
      * `img` - en bild på produkten
      * `p` - en beskrivning av produkten

### Grundkoden

* Börja med grundkoden
* Fyll i alla HTML-element som bygger upp sidan
* Hämta material från [https://www.burgerking.se/menu](https://www.burgerking.se/menu)

![Alt text](../.gitbook/assets/image-1.png)

## Steg 3 - snygga till sidan med CSS

### CSS-reglerna

* Infoga först CSS-reset
* Infoga alla CSS-regler som motsvarar de taggar vi använder
* Gå till [Google Fonts](https://fonts.google.com) och välj liknande typsnitt

```css	
/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
img {
    max-width: 100%;
    height: auto;
}
```

![Alt text](../.gitbook/assets/image-2.png)

![Alt text](../.gitbook/assets/image-3.png)
