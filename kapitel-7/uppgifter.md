
# Riktiga hemsidor att efterlikna

Här är några exempel på riktiga hemsidor som du kan efterlikna.

## [Google Play](https://play.google.com/store/games?hl=sv)

![https://play.google.com/store/games?hl=sv](../.gitbook/assets/image-128.png)

## [SJ](https://www.sj.se/)

![https://www.sj.se/](../.gitbook/assets/image-129.png)