# Klassiska layouter med CSS Grid (del 2)

## Översikt: Semantiska element

**Semantiska element** är HTML‑taggar som inte bara strukturerar innehållet utan även beskriver dess innebörd. Exempelvis:

- **`<header>`**: Innehåller sidans eller en sektions introduktion (t.ex. rubrik, logotyp).
- **`<nav>`**: Innehåller navigationslänkar.
- **`<main>`**: Innehåller det huvudsakliga, unika innehållet på sidan (endast ett per sida, och det ska inte ligga inuti t.ex. `<header>`, `<nav>`, `<aside>`, `<footer>` eller `<article>`).
- **`<article>`**: Markerar en självständig innehållsenhet, exempelvis en bloggartikel eller ett nyhetsinlägg.
- **`<aside>`**: Innehåller kompletterande information eller sidoinnehåll (t.ex. sidofält med extra länkar eller annonser).
- **`<footer>`**: Innehåller sidfotens innehåll, ofta återkommande över sidor (t.ex. copyright‑information).

Att använda dessa element har flera fördelar:

- **Tillgänglighet**: Hjälpmedel som skärmläsare kan enklare navigera och hoppa till huvudinnehållet.
- **SEO**: Sökmotorer kan bättre förstå sidstrukturen.
- **Läsbarhet och underhåll**: Koden blir mer självdokumenterande vilket är bra för både elever och utvecklare.

En viktig regel är att **endast ett `<main>`-element får finnas per dokument** och att det omsluter det innehåll som är unikt för just den sidan.

## Exempel 4: Portfolio‑rutnät (3 kolumner)

```mermaid
block-beta
  columns 3
  header:3
  projekt1 projekt2 projekt3
  projekt4 projekt5 projekt6
  projekt7 projekt8 projekt9
```

Vi antar att sidan har ett gemensamt omslutande element (t.ex. en wrapper med klassen `kontainer`). Här visas sidan med ett `<header>` (med titel) och sedan ett `<main>`-element med alla projekt, där varje projekt ligger i ett `<article>`.

### HTML

```html
<div class="kontainer">
  <header>Portfolio</header>
  <main>
    <article class="card card1">Projekt 1</article>
    <article class="card card2">Projekt 2</article>
    <article class="card card3">Projekt 3</article>
    <article class="card card4">Projekt 4</article>
    <article class="card card5">Projekt 5</article>
    <article class="card card6">Projekt 6</article>
    <article class="card card7">Projekt 7</article>
    <article class="card card8">Projekt 8</article>
    <article class="card card9">Projekt 9</article>
  </main>
</div>
```

### CSS

CSS‑reglerna definierar `<main>` som ett grid med tre kolumner. Varje kort mappas till ett gridområde med sitt namn:

```css
main {
  display: grid;
  grid-template-areas:
    "card1 card2 card3"
    "card4 card5 card6"
    "card7 card8 card9";
  grid-template-columns: repeat(3, 1fr);
  gap: 1em;
  padding: 1em;
}

.card1 { grid-area: card1; background: #f9f9f9; padding: 1em; }
.card2 { grid-area: card2; background: #f9f9f9; padding: 1em; }
.card3 { grid-area: card3; background: #f9f9f9; padding: 1em; }
.card4 { grid-area: card4; background: #f9f9f9; padding: 1em; }
.card5 { grid-area: card5; background: #f9f9f9; padding: 1em; }
.card6 { grid-area: card6; background: #f9f9f9; padding: 1em; }
.card7 { grid-area: card7; background: #f9f9f9; padding: 1em; }
.card8 { grid-area: card8; background: #f9f9f9; padding: 1em; }
.card9 { grid-area: card9; background: #f9f9f9; padding: 1em; }
```

## Exempel 5: Magasinlayout med 3 kolumner

```mermaid
block-beta
  columns 3
  header:3
  nav:3
  featured article1 article2
  article3:3
  footer:3
```

Här ingår gemensamma delar utanför huvudinnehållet. Artiklarna, som utgör det unika innehållet, omsluts av ett `<main>`-element.

### HTML

```html
<div class="kontainer">
  <header>Header</header>
  <nav>Nav</nav>
  <main>
    <article class="featured">Featured Article</article>
    <article class="article1">Artikel 1</article>
    <article class="article2">Artikel 2</article>
    <article class="article3">Artikel 3</article>
  </main>
  <footer>Footer</footer>
</div>
```

### CSS

CSS‑reglerna definierar ett grid med fem rader och tre kolumner, där varje gridområde namnges enligt innehållet:

```css
.kontainer {
  display: grid;
  grid-template-areas:
    "header header header"
    "nav nav nav"
    "featured article1 article2"
    "article3 article3 article3"
    "footer footer footer";
  grid-template-columns: repeat(3, 1fr);
  gap: 1em;
  padding: 1em;
}

header { 
  grid-area: header; 
  background: #eaeaea; 
  padding: 1em; 
}
nav { 
  grid-area: nav; 
  background: #dcdcdc; 
  padding: 1em; 
}
.featured { 
  grid-area: featured; 
  background: #f7f7f7; 
  padding: 1em; 
}
.article1 { 
  grid-area: article1; 
  background: #f0f0f0; 
  padding: 1em; 
}
.article2 { 
  grid-area: article2; 
  background: #f0f0f0; 
  padding: 1em; 
}
.article3 { 
  grid-area: article3; 
  background: #e0e0e0; 
  padding: 1em; 
}
footer { 
  grid-area: footer; 
  background: #d0d0d0; 
  padding: 1em; 
  text-align: center; 
}
```

## Exempel 6: Dashboard-layout med widgets (4 kolumner)

```mermaid
block-beta
  columns 4
  header:4
  widget1 widget2 widget3 widget4
  widget5 widget6 widget7 widget8
  footer:4
```

Widgets, som utgör det centrala innehållet, omsluts av ett `<main>`-element medan header och footer ligger utanför.

### HTML

```html
<div class="kontainer">
  <header>Dashboard Header</header>
  <main>
    <div class="widget widget1">Widget 1</div>
    <div class="widget widget2">Widget 2</div>
    <div class="widget widget3">Widget 3</div>
    <div class="widget widget4">Widget 4</div>
    <div class="widget widget5">Widget 5</div>
    <div class="widget widget6">Widget 6</div>
    <div class="widget widget7">Widget 7</div>
    <div class="widget widget8">Widget 8</div>
  </main>
  <footer>Dashboard Footer</footer>
</div>
```

### CSS

```css
.kontainer {
  display: grid;
  grid-template-areas:
    "header header header header"
    "widget1 widget2 widget3 widget4"
    "widget5 widget6 widget7 widget8"
    "footer footer footer footer";
  grid-template-columns: repeat(4, 1fr);
  gap: 1em;
  padding: 1em;
}

header { 
  grid-area: header; 
  background: #ececec; 
  padding: 1em; 
}
.widget1 { 
  grid-area: widget1;
  background: #f5f5f5; 
  padding: 1em; 
}
.widget2 { 
  grid-area: widget2;
  background: #f5f5f5; 
  padding: 1em; 
}
.widget3 { 
  grid-area: widget3;
  background: #f5f5f5; 
  padding: 1em; 
}
.widget4 { 
  grid-area: widget4;
  background: #f5f5f5; 
  padding: 1em; 
}
.widget5 { 
  grid-area: widget5;
  background: #e5e5e5; 
  padding: 1em; 
}
.widget6 { 
  grid-area: widget6;
  background: #e5e5e5; 
  padding: 1em; 
}
.widget7 { 
  grid-area: widget7;
  background: #e5e5e5; 
  padding: 1em; 
}
.widget8 { 
  grid-area: widget8;
  background: #e5e5e5; 
  padding: 1em; 
}
footer { 
  grid-area: footer; 
  background: #dcdcdc; 
  padding: 1em; 
  text-align: center; 
}
```

## Exempel 7: Blandad layout med sidokolumn och produktrutnät

```mermaid	
block-beta
  columns 2
  header:2
  nav:2
  Sidebar Produktrutnät
  Produktrutnät Produktrutnät
  footer:2
```

Vi antar att detta är en sida där både den unika produktpresentationen och den medföljande sidokolumnen ingår i huvudinnehållet. Eftersom sidebar (ofta en navigations- eller filterpanel) vanligen är återkommande, kan du välja att lägga den utanför `<main>`. Här visas ett alternativ där endast produktrutnätet ligger i `<main>`.

### HTML

```html
<div class="kontainer">
  <header>Header</header>
  <nav>Nav</nav>
  <aside>Sidebar</aside>
  <main>
    <section class="product-grid">
      <article class="product product1">Produkt 1</article>
      <article class="product product2">Produkt 2</article>
      <article class="product product3">Produkt 3</article>
      <article class="product product4">Produkt 4</article>
    </section>
  </main>
  <footer>Footer</footer>
</div>
```

### CSS

Observera att vi i CSS använder klassen `.kontainer` för att definiera layouten:
```css
.kontainer {
  display: grid;
  grid-template-areas:
    "header header header"
    "nav nav nav"
    "aside product-grid product-grid"
    "footer footer footer";
  grid-template-columns: 1fr 2fr 2fr;
  gap: 1em;
  padding: 1em;
}

header { grid-area: header; background: #f0f0f0; padding: 1em; }
nav     { grid-area: nav; background: #e0e0e0; padding: 1em; }
aside   { grid-area: aside; background: #d0d0d0; padding: 1em; }

.product-grid {
  grid-area: product-grid;
  display: grid;
  gap: 1em;
  grid-template-areas:
    "prod1 prod2"
    "prod3 prod4";
  grid-template-columns: 1fr 1fr;
  padding: 1em;
  background: #fafafa;
}

.product1 { grid-area: prod1; background: #fff; padding: 0.5em; }
.product2 { grid-area: prod2; background: #fff; padding: 0.5em; }
.product3 { grid-area: prod3; background: #fff; padding: 0.5em; }
.product4 { grid-area: prod4; background: #fff; padding: 0.5em; }

footer { grid-area: footer; background: #c0c0c0; padding: 1em; text-align: center; }
```

## Exempel 8: E‑handelslayout med tre kolumner

```mermaid
block-beta
  columns 3
  header:3
  nav:3
  Sidebar Produktlista Extra
  footer:3
```

Här utgör den unika produktlistningen och extra information huvudinnehållet, så vi lägger dem inuti ett `<main>`-element medan den återkommande sidofunktionen (filtrering, t.ex.) ligger i ett `<aside>`.

### HTML

```html
<div class="kontainer">
  <header>Shop Header</header>
  <nav>Shop Nav</nav>
  <aside class="shop-sidebar">Filter</aside>
  <main>
    <div class="shop-main">Produktlista</div>
    <div class="shop-extra">Extra info</div>
  </main>
  <footer>Shop Footer</footer>
</div>
```

### CSS

```css
.kontainer" {
  display: grid;
  grid-template-areas:
    "header header header"
    "nav nav nav"
    "shop-sidebar shop-main shop-extra"
    "footer footer footer";
  grid-template-columns: 1fr 3fr 1fr;
  gap: 1em;
  padding: 1em;
}

header {
  grid-area: header;
  background: #f8f8f8; 
  padding: 1em;
}
nav { 
  grid-area: nav; 
  background: #e8e8e8; 
  padding: 1em; 
}
.shop-sidebar { 
  grid-area: shop-sidebar; 
  background: #d8d8d8; 
  padding: 1em; 
}
.shop-main { 
  grid-area: shop-main; 
  background: #f0f0f0; 
  padding: 1em; 
}
.shop-extra { 
  grid-area: shop-extra; 
  background: #d0d0d0; 
  padding: 1em; 
}
footer { 
  grid-area: footer; 
  background: #c8c8c8; 
  padding: 1em; 
  text-align: center; 
}
```

## Pedagogisk förklaring av semantiska element

- **Varför använda semantiska element?**
  - **Tillgänglighet:** Hjälpmedel (som skärmläsare) kan snabbare identifiera och navigera till det huvudsakliga innehållet (t.ex. via ett "hoppa till huvudinnehåll"-länk).
  - **Struktur och överskådlighet:** När du använder element som `<header>`, `<main>`, `<article>`, `<nav>`, `<aside>` och `<footer>`, blir koden lättare att läsa och underhålla. Det blir tydligt vad varje del av sidan representerar.
  - **SEO:** Sökmotorer drar nytta av den semantiska strukturen för att bättre förstå innehållets hierarki och relevans.

- **Regler för några vanliga semantiska element:**
  - **`<main>`:**  
    - Får endast finnas **en gång per sida** och ska omge det innehåll som är unikt för just den sidan.
    - Ska **inte** placeras inuti element som `<header>`, `<nav>`, `<aside>`, `<footer>` eller `<article>`.
  - **`<header>` och `<footer>`:**  
    - Används för att markera sidans (eller en sektions) översta eller nedersta delar. Dessa innehåller ofta återkommande information (t.ex. logotyp, navigationsmeny, kontaktuppgifter).
  - **`<nav>`:**  
    - Markerar navigationslänkar. Hjälper användare att snabbt hitta runt på sidan.
  - **`<article>`:**  
    - Används för innehåll som är självständigt, t.ex. en bloggartikel eller ett nyhetsinlägg.
  - **`<aside>`:**  
    - Innehåller kompletterande information, t.ex. ett sidofält med extra länkar eller annonser, och ligger ofta utanför det primära innehållet.
