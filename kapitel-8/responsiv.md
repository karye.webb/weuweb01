---
description: Hur man gör att webbsidan anpassar sig till skärmens storlek.
---

# Responsiv layout

![alt text](../.gitbook/assets/image-142.png)

Nu går vi vidare och tittar på hur man gör webbsidan responsiv. Det betyder att webbsidan anpassar sig till skärmens storlek.

På en mobiltelefon vill vi tex att sidan blir smalare och att boxarna ligger ovanför varandra istället för bredvid varandra. På en dator med en stor skärm har vi plats att visa mer information utan att det blir trångt.

Statistiken visar att allt fler använder mobiler och läsplattor för att surfa på nätet. Aktuell statistik från [StatCounter](https://gs.statcounter.com/platform-market-share/desktop-mobile-tablet) visar att 55% av all trafik på nätet kommer från mobiler och läsplattor. Det är därför viktigt att webbsidan ser bra ut på alla skärmstorlekar.

## Video 

### Top 7 Most Common Screen Resolutions (2010 - 2020)

{% embed url="https://youtu.be/AWz-NauhhOw?si=PjhzgOh4_8mmedGi" %}

### Learn How to Make a Responsive CSS Grid Layout

{% embed url="https://youtu.be/_gef_lj0FXc?si=byGlDzXx67JTftI9" %}

## Länkar

* [MDN - responsive design](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_grid_layout/Realizing_common_layouts_using_grids)

## Responsiv layout

Med **media queries** kan vi skapa separat CSS-regler för olika skärmstorlekar. Vi kan tex göra så att sidan ser annorlunda ut på en mobiltelefon än på en dator.

Vi utgår från 3 vanliga skärmstorlekar:
- 320 pixlar (mobil)
- 321 - 768 pixlar (läsplatta)
- 769 pixlar och större (dator)

![alt text](../.gitbook/assets/image-147.png)

### För datorer med stora skärmar: >= 769 pixlar

```mermaid
block-beta
  columns 2
  header:2
  main aside
  footer:2
```

![](<../.gitbook/assets/image (70).png>) från ["Making design more responsive with variable breakpoints"](https://uxdesign.cc/making-design-more-responsive-with-variable-breakpoints-4ff038337420)

Avslutningsvis kan layouten vara ännu bredare och "rikare" på en dator med en stor skärm. Vi kan tex göra så att boxarna blir ännu större och att det blir mer utrymme mellan dem. Vår **max-width** är 1000 pixlar.

![alt text](../.gitbook/assets/image-144.png)

### För läsplattor: 577 - 768 pixlar

```mermaid
block-beta
  columns 2
  header:2
  main aside
  footer:2
```

![](<../.gitbook/assets/image (71).png>)

På läsplattor vill vi en lite annan layout. Vi kan tex göra så att sidan blir lite bredare och att boxarna ligger bredvid varandra.

Vi styr detta genom att ange **max-width** på 768 pixlar. Det betyder att reglerna inuti **media queryn** gäller för skärmar som är mellan 576 och 768 pixlar breda.

![alt text](../.gitbook/assets/image-145.png)

### För mobiler: <= 576 pixlar

```mermaid
block-beta
  columns 1
  header
  main
  aside
  footer
```

![](<../.gitbook/assets/image (72).png>)

På mobiler vill vi en lite förenklad layout. Vi kan tex göra så att sidan blir smalare och att boxarna ligger ovanför varandra istället för bredvid varandra.

Vi styr detta genom att ange en **max-width** på 576 pixlar. Det betyder att reglerna inuti **media queryn** gäller för skärmar som är mindre än 576 pixlar breda.

![alt text](../.gitbook/assets/image-146.png)

### Sammanfattning

Vi har nu skapat en responsiv layout som anpassar sig till skärmens storlek. Vi har tre olika layouter beroende på skärmstorlek:

![alt text](../.gitbook/assets/image-143.png)