# Uppgifter: Layouter med CSS Grid

Nedan följer tre uppgifter där du med hjälp av skisser och tips ska lista ut hur layouten är byggd med CSS grid-template-areas. 

## Uppgift 1: Hero-sektion med två kolumner

```mermaid
block-beta
  columns 2
  header:2
  Kolumn1 Kolumn2
  footer:2
```

### Tips

- `header`n (Hero) och footern ska spänna över båda kolumnerna.
- Mellanraden delas upp i två lika breda kolumner (t.ex. 1fr och 1fr).
- Fundera på att använda grid-template-areas med exempelvis:  
  `"hero hero"`  
  `"col1 col2"`  
  `"footer footer"`
- Du kan experimentera med `grid-template-columns` för att sätta kolumnbredden.

## Uppgift 2: Magasinlayout med utstickande Featured-artikel

```mermaid
block-beta
  columns 3
  header:3
  nav:3
  featured article1 article2
  article3:3
  footer:3
```

### Tips

- `header`, `nav` och `footer` ska ligga i egna rader och spänna över alla kolumner.
- Raden med de tre artiklarna ska delas upp i tre kolumner. Här ligger Featured-artikeln i första kolumnen, medan Artikel 1 och Artikel 2 ligger i de två andra.
- Nästa rad innehåller Artikel 3 som spänner över alla kolumner.
- Ett exempel på grid-template-areas kan vara:  
  `"header header header"`  
  `"nav nav nav"`  
  `"featured artikel1 artikel2"`  
  `"artikel3 artikel3 artikel3"`  
  `"footer footer footer"`
- Du bör tänka på att använda `grid-template-columns` (t.ex. `1fr 1fr 1fr`) för att skapa tre lika breda kolumner.

### Uppgift 3: Dashboard med widgets i rutnät

```mermaid
block-beta
  columns 3
  header:3
  widget1 widget2 widget3
  widget4 widget5 widget6
  footer:3
```

### Tips

- Layouten består av fyra rader: header, en rad med tre widgets, en ytterligare rad med tre widgets samt `footer`.
- header och `footer` ska spänna över alla kolumner.
- Widgets placeras i en 3x2-matris. Grid-template-areas kan se ut så här:  
  `"header header header"`  
  `"w1 w2 w3"`  
  `"w4 w5 w6"`  
  `"footer footer footer"`
- Fundera på att använda `grid-template-columns: repeat(3, 1fr);` för att få tre jämna kolumner.
- Du kan lägga till ett gap mellan rutorna med `gap`.

Dessa uppgifter uppmuntrar du att själv analysera hur layouten byggs upp med hjälp av grid-template-areas, kolumn- och radinställningar samt att experimentera med responsiv design om så önskas. Lycka till med övningarna!

## Facit

Se [facit](facit-uppgift-layouter.md) för exempellösningar på uppgifterna.