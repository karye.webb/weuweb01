---
description: Här är en lista på tutorials för att träna på egen hand
---

# Kurser på engelska

## Webben och HTML5 för nybörjare

{% embed url="https://youtu.be/DPnqb74Smug" %}

* Skapa en tom webbrot i VS Code
* Följ instruktionerna och koda i VS Code

## Fördjupning i webbutveckling

### HTML and CSS Tutorial for 2021 - COMPLETE Crash Course!

{% embed url="https://youtu.be/D-h8L5hgW-w?si=ttPBq9DZyKlKAjgG" %}

* Skapa en tom webbrot i VS Code
* Följ instruktionerna och koda i VS Code

{% file src="/.gitbook/assets/2021frontend.zip" %}

## &quot;Learn the Web&quot; webbdesignkurs

![Alt text](../.gitbook/assets/image-8.png)

En mycket bra kurs på engelska som går igenom grunderna i HTML, CSS och Javascript:

{% embed url="https://learntheweb.courses/courses/web-design-1/" %}